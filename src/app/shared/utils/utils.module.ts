import {NgModule} from '@angular/core';
import {DebounceDirective} from './debounce.directive';

@NgModule({
  declarations: [DebounceDirective],
  providers: [DebounceDirective],
  exports: [DebounceDirective]
})
export class UtilsModule {
}

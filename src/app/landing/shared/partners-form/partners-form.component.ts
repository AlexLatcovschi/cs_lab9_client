import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NbToastrService } from '@nebular/theme';
import { Observable } from 'rxjs';
import { PartnersRequestService } from './partners-request.service';

@Component({
  selector: 'app-partners-form',
  templateUrl: './partners-form.component.html',
  styleUrls: ['./partners-form.component.scss']
})
export class PartnersFormComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder,
              private service: PartnersRequestService,
              private toastrService: NbToastrService) {
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      company: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', [Validators.required]]
    });
  }

  get f() {
    return this.registerForm.controls;
  }

  onSubmit(): Observable<any> {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    } else {
      this.service.sendPartnerRequest(this.registerForm.value).subscribe(() => {
        this.showToast('top-right', 'success');
      }, () => {
        this.errorToast('top-right', 'danger');
      });
    }
  }

  showToast(position, status) {
    this.toastrService.show(
      status || 'success',
      `Form added successfully`,
      { position, status });
  }

  errorToast(position, status) {
    this.toastrService.show(
      status || 'danger',
      `An error occurred!`,
      { position, status });
  }
}

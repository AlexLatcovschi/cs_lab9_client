import { Component } from '@angular/core';

@Component({
  selector: 'app-partners-logo',
  templateUrl: './partners.component.html',
  styleUrls: ['./partners.component.scss']
})
export class PartnersComponent {
  partners = [
    '../../../../assets/images/partners/1.svg',
    '../../../../assets/images/partners/2.svg',
    '../../../../assets/images/partners/3.svg',
    '../../../../assets/images/partners/4.svg',
    '../../../../assets/images/partners/5.svg',
    '../../../../assets/images/partners/6.svg',
    '../../../../assets/images/partners/7.svg',
    '../../../../assets/images/partners/8.svg'
  ];

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sidebar/sidebar.component';
import { RouterModule } from '@angular/router';
import { FooterComponent } from './footer/footer.component';
import { SlickComponent } from './slick/slick.component';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { PartnersComponent } from './partners/partners.component';
import { PartnersFormComponent } from './partners-form/partners-form.component';
import { NbCardModule, NbToastrModule } from '@nebular/theme';
import { ReactiveFormsModule } from '@angular/forms';
import { PartnersRequestService } from './partners-form/partners-request.service';
import { SocialMediaComponent } from './social-media/social-media.component';
import { CategoryHeaderComponent } from './category-header/category-header.component';
import { ProductCardComponent } from './product-card/product-card.component';
import { RelatedProductsComponent } from './related-products/related-products.component';


@NgModule({
  declarations: [
    SidebarComponent,
    FooterComponent,
    SlickComponent, PartnersComponent,
    PartnersFormComponent,
    SocialMediaComponent,
    CategoryHeaderComponent,
    ProductCardComponent,
    RelatedProductsComponent],
  exports: [
    SidebarComponent,
    FooterComponent,
    SlickComponent,
    PartnersComponent,
    PartnersFormComponent,
    SocialMediaComponent,
    CategoryHeaderComponent,
    ProductCardComponent,
    RelatedProductsComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    SlickCarouselModule,
    NbCardModule,
    ReactiveFormsModule,
    NbToastrModule.forRoot()
  ],
  providers: [PartnersRequestService]
})
export class SharedModule {
}

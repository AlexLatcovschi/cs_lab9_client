import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-related-products',
  templateUrl: './related-products.component.html',
  styleUrls: ['./related-products.component.scss']
})
export class RelatedProductsComponent implements OnInit {
  @Input() products;

  ngOnInit() {
    this.products = this.products.slice(0, 4);
  }
}

import { Component } from '@angular/core';
import { SIDEBAR_TABS } from '../../landing-constants';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {
  sidebarTabs = SIDEBAR_TABS;

}

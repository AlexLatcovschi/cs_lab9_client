import { Component } from '@angular/core';

@Component({
  selector: 'app-social-media',
  templateUrl: './social-media.component.html',
  styleUrls: ['./social-media.component.scss']
})
export class SocialMediaComponent {
  socialLinks = [
    {
      icon: '../../../../assets/images/instagram-white.png',
      href: ''
    },
    {
      icon: '../../../../assets/images/yt-white.png',
      href: ''
    },
    {
      icon: '../../../../assets/images/fb-white.png',
      href: ''
    }
  ];
  socialImages = [
    'http://placehold.it/350x150/000000',
    'http://placehold.it/350x150/000000',
    'http://placehold.it/350x150/000000',
    'http://placehold.it/350x150/000000',
    'http://placehold.it/350x150/000000',
    'http://placehold.it/350x150/000000',
    'http://placehold.it/350x150/000000',
    'http://placehold.it/350x150/000000',
    'http://placehold.it/350x150/000000'
  ];

}

import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { SIDEBAR_TABS } from '../../landing-constants';
import * as L from 'leaflet';
import 'mapbox-gl-leaflet';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'main-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit, AfterViewInit {
  sidebarTabs = SIDEBAR_TABS;
  contacts = [
    {
      icon: '../../../../assets/images/phone.svg',
      title: '+373 00000000',
      url: 'tel:+37300000000'
    },
    {
      icon: '../../../../assets/images/mail.svg',
      title: 'ami@mail.ru',
      url: 'mailto:ami@mail.ru'
    },
    {
      icon: '../../../../assets/images/location.svg',
      title: '2972 Westheimer Rd. Santa Ana, Illinois 85486 ',
      url: '',
      coordinates: []
    }
  ];
  social = [
    {
      icon: '../../../../assets/images/instagram-white.png',
      url: ''
    },
    {
      icon: '../../../../assets/images/yt-white.png',
      url: ''
    },
    {
      icon: '../../../../assets/images/fb-white.png',
      url: ''
    },
    {
      icon: '../../../../assets/images/vk-white.png',
      url: ''
    }
  ];
  @ViewChild('map')
  private mapContainer: ElementRef<HTMLElement>;
  private map: L.Map;

  ngOnInit() {
    this.sidebarTabs = JSON.parse(JSON.stringify(this.sidebarTabs));
    this.sidebarTabs.push({
      name: 'Скачать каталог .pdf',
      url: ''
    });
  }

  ngAfterViewInit() {
    const myAPIKey = 'db3518dad9504eea90a441f85abb4a47';
    const mapStyle = 'https://maps.geoapify.com/v1/styles/osm-carto/style.json';

    const initialState = {
      lng: 11,
      lat: 49,
      zoom: 4
    };

    const map = new L.Map(this.mapContainer.nativeElement).setView(
      [initialState.lat, initialState.lng],
      initialState.zoom
    );

    // the attribution is required for the Geoapify Free tariff plan
    map.attributionControl
      .setPrefix('')
      .addAttribution(
        'Powered by <a href="https://www.geoapify.com/" target="_blank">Geoapify</a> | © OpenStreetMap <a href="https://www.openstreetmap.org/copyright" target="_blank">contributors</a>'
      );

    L.mapboxGL({
      style: `${mapStyle}?apiKey=${myAPIKey}`,
      accessToken: 'no-token'
    }).addTo(map);
  }
}

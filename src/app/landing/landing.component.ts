import { Component } from '@angular/core';

@Component({
  selector: 'app-landing',
  template: `
    <nb-layout>
      <nb-layout-column class="landing-layout">
        <div class="page-layout">
          <div class="left-child">
            <app-sidebar></app-sidebar>
          </div>
          <div class="right-child">
            <router-outlet></router-outlet>
            <main-footer></main-footer>
          </div>
        </div>
      </nb-layout-column>
    </nb-layout>
  `,
  styleUrls: ['./landing.scss']

})
export class LandingComponent {
}


import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BlogPageComponent } from './blog-page.component';
import { RouterModule } from '@angular/router';
import { NbCardModule, NbFormFieldModule, NbIconModule } from '@nebular/theme';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { SharedModule } from '../shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [BlogPageComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: BlogPageComponent
      }
    ]),
    NbCardModule,
    NgxDatatableModule,
    SlickCarouselModule,
    NbIconModule,
    NbFormFieldModule,
    SharedModule,
    NgbModule
  ]
})
export class BlogPageModule {
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { KidsPageComponent } from './kids-page.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [KidsPageComponent],
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: KidsPageComponent
      }
    ]),
    CommonModule,
    SharedModule
  ]
})
export class KidsPageModule { }

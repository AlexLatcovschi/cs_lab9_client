import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SingleProductComponent } from './single-product.component';
import { RouterModule } from '@angular/router';
import { ProductCategoryComponent } from '../product-category/product-category.component';
import { NbCardModule, NbFormFieldModule, NbIconModule } from '@nebular/theme';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { SharedModule } from '../shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [SingleProductComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: SingleProductComponent
      }
    ]),
    NbCardModule,
    NgxDatatableModule,
    SlickCarouselModule,
    NbIconModule,
    NbFormFieldModule,
    SharedModule,
    NgbModule]
})
export class SingleProductModule {
}

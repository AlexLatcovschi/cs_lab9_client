import { Component } from '@angular/core';
import { NbToastrService } from '@nebular/theme';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-single-product',
  templateUrl: './single-product.component.html',
  styleUrls: ['./single-product.component.scss']
})
export class SingleProductComponent {
  constructor(private toastrService: NbToastrService, private cookie: CookieService) {
  }

  products = [
    {
      title: 'Сахарное печенье DINO 300 г',
      image: 'http://placehold.it/350x150/000000',
      url: '#',
      id: '1'
    },
    {
      title: 'Сахарное печенье DINO 300 г',
      image: 'http://placehold.it/350x150/000000',
      url: '#',
      id: '1'
    },
    {
      title: 'Сахарное печенье DINO 300 г',
      image: 'http://placehold.it/350x150/000000',
      url: '#',
      id: '1'
    },
    {
      title: 'Сахарное печенье DINO 300 г',
      image: 'http://placehold.it/350x150/000000',
      url: '#',
      id: '1'
    }
  ];

  product = {
    title: 'Сахарное печенье DINO 300 г',
    image: 'http://placehold.it/350x150/000000',
    quantity: [
      {
        value: '200g',
        description: '111Печенье «Формула успеха» с кунжутом является популярным и любимым среди потребителей продуктом. Уникальность вкусу придает золотистый кунжут, которым мы щедро посыпаем печенье.\n' +
          '\n' +
          'Cостав:  Мука пшеничная, масло растительное, сахар, сироп инвертный (сахар, регуляторы кислотности: молочная кислота, гидрокарбонат натрия), крахмал кукурузный, разрыхлители: гидрокарбонат аммония (соль углеаммонийная), гидрокарбонат натрия (сода пищевая); молоко топленое, соль, эмульгаторы: лецитин соевый, эфиры глицерина Кол-во в коробке:                              25 шт\n' +
          'Размер коробки (д * ш * в):        400×292×253\n' +
          'Штрих-код:                                            4607174180659\n',
        images: ['http://placehold.it/350x150/000000', 'http://placehold.it/350x150/111111', 'http://placehold.it/350x150/222222']
      },
      {
        value: '250g',
        description: '222Печенье «Формула успеха» с кунжутом является популярным и любимым среди потребителей продуктом. Уникальность вкусу придает золотистый кунжут, которым мы щедро посыпаем печенье.\n' +
          '\n' +
          'Cостав:  Мука пшеничная, масло растительное, сахар, сироп инвертный (сахар, регуляторы кислотности: молочная кислота, гидрокарбонат натрия), крахмал кукурузный, разрыхлители: гидрокарбонат аммония (соль углеаммонийная), гидрокарбонат натрия (сода пищевая); молоко топленое, соль, эмульгаторы: лецитин соевый, эфиры глицерина Кол-во в коробке:                              25 шт\n' +
          'Размер коробки (д * ш * в):        400×292×253\n' +
          'Штрих-код:                                            4607174180659\n',
        images: ['http://placehold.it/350x150/000000', 'http://placehold.it/350x150/000000', 'http://placehold.it/350x150/000000']
      },
      {
        value: '300g',
        description: '333Печенье «Формула успеха» с кунжутом является популярным и любимым среди потребителей продуктом. Уникальность вкусу придает золотистый кунжут, которым мы щедро посыпаем печенье.\n' +
          '\n' +
          'Cостав:  Мука пшеничная, масло растительное, сахар, сироп инвертный (сахар, регуляторы кислотности: молочная кислота, гидрокарбонат натрия), крахмал кукурузный, разрыхлители: гидрокарбонат аммония (соль углеаммонийная), гидрокарбонат натрия (сода пищевая); молоко топленое, соль, эмульгаторы: лецитин соевый, эфиры глицерина Кол-во в коробке:                              25 шт\n' +
          'Размер коробки (д * ш * в):        400×292×253\n' +
          'Штрих-код:                                            4607174180659\n',
        images: ['http://placehold.it/350x150/000000', 'http://placehold.it/350x150/000000', 'http://placehold.it/350x150/000000']
      }
    ],
    id: '1'
  };
  currentType = this.product.quantity[0];
  currentImage = this.product.quantity[0].images[0];

  addProduct() {
    let cookieValue = [];
    if (this.cookie.get('ami_products_list')) {
      cookieValue = [...JSON.parse(this.cookie.get('ami_products_list'))];
    }
    const foundId = cookieValue.find(el => el === this.product.id);
    if (!foundId) {
      cookieValue.push(this.product.id);
    }
    if (cookieValue) {
      this.cookie.set('ami_products_list', JSON.stringify(cookieValue));
    }
    this.toastrService.show(
      'success',
      `User added successfully`);
  }

  setProductType(type) {
    this.currentType = this.product.quantity.find(el => el.value === type.target.id);
  }

  setCurrentImage(image) {
    this.currentImage = image;
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { OrderDoneComponent } from './order-done.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [OrderDoneComponent],
  imports: [
    RouterModule.forChild([
    {
      path: '',
      component: OrderDoneComponent
    }
  ]),
    CommonModule,
    SharedModule
  ]
})
export class OrderDoneModule { }

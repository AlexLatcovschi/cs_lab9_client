export const SIDEBAR_TABS = [
  {
    name: 'Каталог',
    url: '/products'
  },
  {
    name: 'Партнерам',
    url: '/for-partners'
  },
  {
    name: 'блог',
    url: '/blog'
  },
  {
    name: 'for kids',
    url: '/kids-page'
  }
];

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { BlogComponent } from './blog.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [BlogComponent],
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: BlogComponent
      }
    ]),
    CommonModule,
    SharedModule,
    NgbModule
  ]
})
export class BlogModule { }

import { Component, OnInit, Output } from '@angular/core';
import { EventEmitter } from 'events';

@Component({
  selector: 'app-product-category',
  templateUrl: './product-category.component.html',
  styleUrls: ['./product-category.component.scss']
})
export class ProductCategoryComponent implements OnInit {
  productCategoryTabs = [
    {
      id: '1',
      title: 'Печенье'
    },
    {
      id: '2',
      title: 'Мармелад'
    },
    {
      id: '3',
      title: 'Маршмеллоу'
    },
    {
      id: '4',
      title: 'Шоколад'
    },
    {
      id: '5',
      title: 'Вафли'
    }
  ];
  selectedFilters = [];
  filters = [
    {
      name: 'Бренд',
      column: '',
      items: [
        {
          name: 'Вся продукция1',
          value: '',
          checked: false,
          id: '1'
        },
        {
          name: 'Вся продукция2',
          value: '',
          checked: false,
          id: '2'
        },
        {
          name: 'Вся продукция3',
          value: '',
          checked: false,
          id: '3'
        },
        {
          name: 'Вся продукция4',
          value: '',
          checked: false,
          id: '4'
        },
        {
          name: 'Вся продукция5',
          value: '',
          checked: false,
          id: '5'
        },
        {
          name: 'Вся продукция6',
          value: '',
          checked: false,
          id: '6'
        }
      ]
    },
    {
      name: 'Тип печенья',
      column: '',
      items: [
        {
          name: 'Вся продукция11',
          value: '',
          checked: false,
          id: '1'
        },
        {
          name: 'Вся продукция22',
          value: '',
          checked: false,
          id: '2'
        },
        {
          name: 'Вся продукция33',
          value: '',
          checked: false,
          id: '3'
        },
        {
          name: 'Вся продукция44',
          value: '',
          checked: false,
          id: '4'
        },
        {
          name: 'Вся продукция55',
          value: '',
          checked: false,
          id: '5'
        },
        {
          name: 'Вся продукция66',
          value: '',
          checked: false,
          id: '6'
        }
      ]
    },
    {
      name: 'Вес',
      column: '',
      items: [
        {
          name: 'Вся продукция111',
          value: '',
          checked: false,
          id: '1'
        },
        {
          name: 'Вся продукция222',
          value: '',
          checked: false,
          id: '2'
        },
        {
          name: 'Вся продукция333',
          value: '',
          checked: false,
          id: '3'
        },
        {
          name: 'Вся продукция444',
          value: '',
          checked: false,
          id: '4'
        },
        {
          name: 'Вся продукция555',
          value: '',
          checked: false,
          id: '5'
        },
        {
          name: 'Вся продукция666',
          value: '',
          checked: false,
          id: '6'
        }
      ]
    },
    {
      name: 'Состав',
      column: '',
      items: [
        {
          name: 'Вся продукция1111',
          value: '',
          checked: false,
          id: '1'
        },
        {
          name: 'Вся продукция2222',
          value: '',
          checked: false,
          id: '2'
        },
        {
          name: 'Вся продукция3333',
          value: '',
          checked: false,
          id: '3'
        },
        {
          name: 'Вся продукция4444',
          value: '',
          checked: false,
          id: '4'
        },
        {
          name: 'Вся продукция5555',
          value: '',
          checked: false,
          id: '5'
        },
        {
          name: 'Вся продукция6666',
          value: '',
          checked: false,
          id: '6'
        }
      ]
    },
    {
      name: 'Кол. в упаковке',
      column: '',
      items: [
        {
          name: 'Вся продукция11111',
          value: '',
          checked: false,
          id: '1'
        },
        {
          name: 'Вся продукция22222',
          value: '',
          checked: false,
          id: '2'
        },
        {
          name: 'Вся продукция33333',
          value: '',
          checked: false,
          id: '3'
        },
        {
          name: 'Вся продукция44444',
          value: '',
          checked: false,
          id: '4'
        },
        {
          name: 'Вся продукция55555',
          value: '',
          checked: false,
          id: '5'
        },
        {
          name: 'Вся продукция66666',
          value: '',
          checked: false,
          id: '6'
        }
      ]
    }
  ];
  products = [
    {
      title: 'Сахарное печенье DINO 300 г',
      image: 'http://placehold.it/350x150/000000',
      url: '#',
      id: '1'
    },
    {
      title: 'Сахарное печенье DINO 300 г',
      image: 'http://placehold.it/350x150/000000',
      url: '#',
      id: '1'
    },
    {
      title: 'Сахарное печенье DINO 300 г',
      image: 'http://placehold.it/350x150/000000',
      url: '#',
      id: '1'
    },
    {
      title: 'Сахарное печенье DINO 300 г',
      image: 'http://placehold.it/350x150/000000',
      url: '#',
      id: '1'
    },
    {
      title: 'Сахарное печенье DINO 300 г',
      image: 'http://placehold.it/350x150/000000',
      url: '#',
      id: '1'
    },
    {
      title: 'Сахарное печенье DINO 300 г',
      image: 'http://placehold.it/350x150/000000',
      url: '#',
      id: '1'
    },
    {
      title: 'Сахарное печенье DINO 300 г',
      image: 'http://placehold.it/350x150/000000',
      url: '#',
      id: '1'
    }
  ];
  count = 110;
  pagination = [];
  page = 1;
  selectedPage;
  paginationUI = [];
  currentPage;
  @Output() pageChanged = new EventEmitter();

  ngOnInit() {
    if (this.count > 0) {
      this.generatePagination();
    }
  }

  addFilter(filter, checked) {
    this.filters.map(el => {
      if (el.items) {
        el.items.map(fil => {
          if (fil.name === filter.name && fil.id === filter.id) {
            fil.checked = !fil.checked;
            if (fil.checked) {
              const founded = this.selectedFilters.find(find => find.name === filter.name && find.id === filter.id);
              if (!founded) {
                this.selectedFilters.push(filter);
              }
            }
          }
        });
      }
    });
  }

  removeSelectedFilter(filter) {
    this.selectedFilters = this.selectedFilters.filter(el => {
      return el.id !== filter.id && el.name !== filter.name;
    });
    this.filters.map(el => {
      if (el.items) {
        el.items.map(fil => {
          if (fil.name === filter.name && fil.id === filter.id) {
            fil.checked = false;
          }
        });
      }
    });
    const selectedFilter = document.getElementById(filter.name + '_' + filter.id);
    selectedFilter.focus();
    selectedFilter.click();
  }

  generatePagination() {
    this.pagination = [];
    this.page = 1;
    for (let i = 0; i < this.count; i++) {
      if ((i % 10) === 0) {
        this.pagination.push({ page: this.page++ });
      }
    }
    if (this.pagination.length > 1) {
      this.generatePaginationUI();
    }
  }

  generatePaginationUI() {
    this.paginationUI = [];
    this.currentPage = this.selectedPage;
    if (!this.currentPage) {
      this.currentPage = 1;
    }
    this.paginationUI.push({ content: this.pagination[0].page });
    if (this.currentPage < 3) {
      for (let i = 1; i < 6; i++) {
        if (this.pagination[i]) {
          this.paginationUI.push({ content: this.pagination[i].page });
        }
      }
      this.paginationUI.push({ content: '...' });
    } else if (this.currentPage >= 3 && this.currentPage <= this.pagination.length - 3) {
      this.paginationUI.push({ content: '...' });
      for (let i = this.currentPage - 3; i < this.currentPage + 2; i++) {
        if (this.pagination[i]) {
          this.paginationUI.push({ content: this.pagination[i].page });
        }
      }
      this.paginationUI.push({ content: '...' });
    } else if (this.currentPage <= this.pagination.length && this.currentPage > this.pagination.length - 6) {
      this.paginationUI.push({ content: '...' });
      for (let i = this.pagination.length - 6; i < this.pagination.length - 1; i++) {
        if (this.pagination[i]) {
          this.paginationUI.push({ content: this.pagination[i].page });
        }
      }
    }
    this.paginationUI.push({ content: this.pagination[this.pagination.length - 1].page });
  }

  onPageSelected(event) {
    this.selectedPage = Number(event.srcElement.innerText);
    this.pageChanged.emit(this.selectedPage);
    this.generatePaginationUI();
  }

  goToPrevious() {
    const currentActionPage = this.selectedPage--;
    if (currentActionPage >= 0) {
      // this.pageChanged.emit(currentActionPage);
    }
    this.generatePaginationUI();
  }

  goToNext() {
    const currentActionPage = this.selectedPage++;
    if (currentActionPage <= this.page) {
      // this.pageChanged.emit(currentActionPage);
    }
    this.generatePaginationUI();
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductCategoryComponent } from './product-category.component';
import { RouterModule } from '@angular/router';
import { NbAccordionModule, NbCardModule, NbCheckboxModule, NbFormFieldModule, NbIconModule } from '@nebular/theme';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { SharedModule } from '../shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [ProductCategoryComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: ProductCategoryComponent
      }
    ]),
    NbCardModule,
    NgxDatatableModule,
    SlickCarouselModule,
    NbIconModule,
    NbFormFieldModule,
    SharedModule,
    NgbModule,
    NbAccordionModule,
    NbCheckboxModule
  ]
})
export class ProductCategoryModule {
}

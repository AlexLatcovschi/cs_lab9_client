import { NgModule } from '@angular/core';
import { LandingComponent } from './landing.component';
import { RouterModule } from '@angular/router';
import { NbButtonModule, NbIconModule, NbInputModule, NbLayoutModule, NbRouteTabsetModule } from '@nebular/theme';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from './shared/shared.module';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: LandingComponent,
        children: [
          {
            path: '',
            loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
          },
          {
            path: 'order-done',
            loadChildren: () => import('./order-done/order-done.module').then(m => m.OrderDoneModule)
          },
          {
            path: 'blog/:id',
            loadChildren: () => import('./blog/blog.module').then(m => m.BlogModule)
          },
          {
            path: 'blog',
            loadChildren: () => import('./blog-page/blog-page.module').then(m => m.BlogPageModule)
          },
          {
            path: 'products',
            loadChildren: () => import('./product-category/product-category.module').then(m => m.ProductCategoryModule)
          },
          {
            path: 'products/:id',
            loadChildren: () => import('./single-product/single-product.module').then(m => m.SingleProductModule)
          },
          {
            path: 'for-partners',
            loadChildren: () => import('./for-partners/for-partners.module').then(m => m.ForPartnersModule)
          },
          {
            path: 'kids-page',
            loadChildren: () => import('./kids-page/kids-page.module').then(m => m.KidsPageModule)
          },
          {
            path: 'checkout',
            loadChildren: () => import('./checkout/checkout.module').then(m => m.CheckoutModule)
          }
        ]
      }
    ]),
    NbLayoutModule,
    CommonModule,
    NbRouteTabsetModule,
    NbIconModule,
    NbInputModule,
    NgbModule,
    NbButtonModule,
    SharedModule
  ],
  declarations: [LandingComponent],
  exports: []
})
export class LandingModule {
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ForPartnersComponent } from './for-partners.component';
import { SharedModule } from '../shared/shared.module';
import { AmiEverywhereComponent } from './ami-everywhere/ami-everywhere.component';



@NgModule({
  declarations: [ForPartnersComponent, AmiEverywhereComponent],
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: ForPartnersComponent
      }
    ]),
    CommonModule,
    SharedModule
  ]
})
export class ForPartnersModule { }

import { Component } from '@angular/core';

@Component({
  selector: 'app-ami-everywhere',
  templateUrl: './ami-everywhere.component.html',
  styleUrls: ['./ami-everywhere.component.scss']
})
export class AmiEverywhereComponent {
  images = [
    'http://placehold.it/350x150/000000',
    'http://placehold.it/350x150/000000',
    'http://placehold.it/350x150/000000',
    'http://placehold.it/350x150/000000'
  ];

}

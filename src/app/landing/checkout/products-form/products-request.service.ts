import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

@Injectable()
export class ProductsRequestService {

  constructor(private http: HttpClient) {
  }

  sendPartnerRequest(partner: any) {
    return this.http.post(environment.api_url + '/api/products-request', partner);
  }
}

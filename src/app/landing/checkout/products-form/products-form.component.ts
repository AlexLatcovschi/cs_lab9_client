import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NbToastrService } from '@nebular/theme';
import { Observable } from 'rxjs';
import { ProductsRequestService } from './products-request.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-products-form',
  templateUrl: './products-form.component.html',
  styleUrls: ['./products-form.component.scss']
})
export class ProductsFormComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder,
              private service: ProductsRequestService,
              private toastrService: NbToastrService,
              private cookie: CookieService) {
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      address: ['', [Validators.required]],
      phone: ['', [Validators.required]],
      fullName: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      text: ['', [Validators.required]]
    });
  }

  get f() {
    return this.registerForm.controls;
  }

  onSubmit(): Observable<any> {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    } else {
      let cookieProducts;
      if (this.cookie.get('ami_products_list')) {
        cookieProducts = JSON.parse(this.cookie.get('ami_products_list'));
      }
      console.log('cookieProducts: ', cookieProducts);
      this.service.sendPartnerRequest({ ...this.registerForm.value, products: cookieProducts }).subscribe(() => {
        this.showToast('top-right', 'success');
      }, () => {
        this.errorToast('top-right', 'danger');
      });
    }
  }

  showToast(position, status) {
    this.toastrService.show(
      status || 'success',
      `Order added successfully`,
      { position, status });
  }

  errorToast(position, status) {
    this.toastrService.show(
      status || 'danger',
      `An error occurred!`,
      { position, status });
  }
}

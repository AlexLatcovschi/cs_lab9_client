import { Component } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent {
  constructor(private cookie: CookieService) {
  }

  products = [
    {
      title: 'Сахарное печенье DINO 300 г',
      image: 'http://placehold.it/350x150/000000',
      url: '#',
      id: '1',
      count: 0,
      type: '400g'
    },
    {
      title: 'Сахарное печенье DINO 300 г',
      image: 'http://placehold.it/350x150/000000',
      url: '#',
      id: '2',
      count: 0,
      type: '400g'
    },
    {
      title: 'Сахарное печенье DINO 300 г',
      image: 'http://placehold.it/350x150/000000',
      url: '#',
      id: '3',
      count: 0,
      type: '400g'
    },
    {
      title: 'Сахарное печенье DINO 300 г',
      image: 'http://placehold.it/350x150/000000',
      url: '#',
      id: '4',
      count: 0,
      type: '400g'
    },
    {
      title: 'Сахарное печенье DINO 300 г',
      image: 'http://placehold.it/350x150/000000',
      url: '#',
      id: '5',
      count: 0,
      type: '400g'
    },
    {
      title: 'Сахарное печенье DINO 300 г',
      image: 'http://placehold.it/350x150/000000',
      url: '#',
      id: '6',
      count: 0,
      type: '400g'
    },
    {
      title: 'Сахарное печенье DINO 300 г',
      image: 'http://placehold.it/350x150/000000',
      url: '#',
      id: '7',
      count: 0,
      type: '400g'
    }
  ];

  getCount(type, item) {
    let count = item.count;
    if (type === 'decrease') {
      if (count > 0) {
        count--;
        return count;
      } else {
        return count;
      }
    }
    if (type === 'increase') {
      count++;
      return count;
    }
  }

  removeItem(item) {
    this.products = this.products.filter(el => el.id !== item.id);
    let cookieProducts = JSON.parse(this.cookie.get('ami_products_list'));
    cookieProducts = cookieProducts.filter(el => el.id !== item.id);
    this.cookie.set('ami_products_list', JSON.stringify(cookieProducts));
  }
}

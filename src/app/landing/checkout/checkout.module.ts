import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CheckoutComponent } from './checkout.component';
import { RouterModule } from '@angular/router';
import { BlogPageComponent } from '../blog-page/blog-page.component';
import { NbCardModule, NbFormFieldModule, NbIconModule } from '@nebular/theme';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { SharedModule } from '../shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ProductsFormComponent } from './products-form/products-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ProductsRequestService } from './products-form/products-request.service';


@NgModule({
  declarations: [CheckoutComponent, ProductsFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: CheckoutComponent
      }
    ]),
    NbCardModule,
    NgxDatatableModule,
    SlickCarouselModule,
    NbIconModule,
    NbFormFieldModule,
    SharedModule,
    NgbModule,
    ReactiveFormsModule
  ],
  providers: [ProductsRequestService]
})
export class CheckoutModule {
}

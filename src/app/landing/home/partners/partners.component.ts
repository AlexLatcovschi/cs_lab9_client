import { Component } from '@angular/core';

@Component({
  selector: 'app-partners',
  templateUrl: './partners.component.html',
  styleUrls: ['./partners.component.scss']
})
export class PartnersComponent {
  partners = [
    {
      title: 'Dolce Dimineața',
      image: 'http://placehold.it/350x150/000000',
      url: '#'
    },
    {
      title: 'Natura',
      image: 'http://placehold.it/350x150/000000',
      url: '#'
    },
    {
      title: 'DINO',
      image: 'http://placehold.it/350x150/000000',
      url: '#'
    },
    {
      title: 'Champion',
      image: 'http://placehold.it/350x150/000000',
      url: '#'
    },
    {
      title: 'Hrust',
      image: 'http://placehold.it/350x150/000000',
      url: '#'
    },
    {
      title: 'Energy',
      image: 'http://placehold.it/350x150/000000',
      url: '#'
    },
    {
      title: 'Galaxy',
      image: 'http://placehold.it/350x150/000000',
      url: '#'
    }
  ];
}

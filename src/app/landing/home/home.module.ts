import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeComponent} from './home.component';
import {RouterModule} from '@angular/router';
import {NbCardModule, NbFormFieldModule, NbIconModule} from '@nebular/theme';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {SlickCarouselModule} from 'ngx-slick-carousel';
import { SharedModule } from '../shared/shared.module';
import { PartnersComponent } from './partners/partners.component';
import { NewsBlockComponent } from './news-block/news-block.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomeComponent
      }
    ]),
    NbCardModule,
    NgxDatatableModule,
    SlickCarouselModule,
    NbIconModule,
    NbFormFieldModule,
    SharedModule
  ],
  exports: [
    PartnersComponent
  ],
  declarations: [HomeComponent, PartnersComponent, NewsBlockComponent]
})
export class HomeModule {
}

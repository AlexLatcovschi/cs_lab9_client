import { Component } from '@angular/core';

@Component({
  selector: 'app-news-block',
  templateUrl: './news-block.component.html',
  styleUrls: ['./news-block.component.scss']
})
export class NewsBlockComponent {
  news = [
    {
      title: 'Качественные ингредиенты',
      content: 'Из отборного зерна и высококачественных ингредиентов мы с любовью и заботой создаем продукты «AMI»',
      image: 'http://placehold.it/350x150/000000',
      position: 'left'
    },
    {
      title: 'Необходимые минералы',
      content: 'Печенье AMI насытит организм необходимыми минералами и витаминами.  В составе печенья есть необходимые человеку микроэлементы – железо, цинк, магний, йод, фосфор, натрий',
      image: 'http://placehold.it/350x150/000000',
      position: 'right'
    },
    {
      title: 'Подходит для детей',
      content: 'Наше печенье не только источник необходимых микроэлементов, но и источник развития для вашего ребенка',
      image: 'http://placehold.it/350x150/000000',
      position: 'left'
    },
    {
      title: 'Особый рецепт',
      content: 'Печенье “AMI” готовится по особенному рецепту, что даст возможность ощутить себя в гостях у бабушки',
      image: 'http://placehold.it/350x150/000000',
      position: 'right'
    }
  ];

}

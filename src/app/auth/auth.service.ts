import {Injectable} from '@angular/core';
import {NbTokenService} from '@nebular/auth';
import {Router} from '@angular/router';
import {AUTH_LOGIN_URL} from './auth.constants';


@Injectable()
export class AuthService {
  constructor(private tokenService: NbTokenService, private router: Router, private NbJwt: NbTokenService) {
  }

  public isAuthenticated(): boolean {
    console.log('AAAAA');
    const token = this.NbJwt.get();
    // Check whether the token is expired and return
    // true or false
    return !!token;
  }

  logout() {
    this.tokenService.clear();
    this.router.navigate([AUTH_LOGIN_URL]);
  }
}

import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { NbAuthService, NbTokenService } from '@nebular/auth';
import { tap } from 'rxjs/operators';
import { AUTH_LOGIN_URL } from './auth.constants';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private authService: NbAuthService, private router: Router, private tokenService: NbTokenService) {
  }

  canActivate() {
    console.log('BBBB', this.tokenService.get());
    this.tokenService.get().subscribe(res => {
      document.getElementById('auth-token').innerText = JSON.stringify(res);
    });
    return this.authService.isAuthenticated().pipe(
      tap(authenticated => {
        // this.tokenService.clear();
        if (!authenticated) {
          this.router.navigate([AUTH_LOGIN_URL]);
        }
      })
    );
  }
}


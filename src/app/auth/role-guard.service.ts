import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router} from '@angular/router';
import {AuthService} from './auth.service';

import {NbTokenService} from '@nebular/auth';
import {AUTH_LOCAL_TOKEN_KEY, AUTH_LOGIN_URL} from './auth.constants';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Injectable()
export class RoleGuard implements CanActivate {
  constructor(public auth: AuthService, public router: Router, private NbJwt: NbTokenService) {
  }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    // this will be passed from the route config
    // on the data property
    const expectedRole = route.data.expectedRole;
   return this.NbJwt.get().pipe(
     map(token => {
       // decode the token to get its payload
       const tokenPayload = token.getPayload();
       if (
         // !this.auth.isAuthenticated() ||
         tokenPayload.roles.includes(expectedRole)
       ) {
         localStorage.removeItem(AUTH_LOCAL_TOKEN_KEY);
         this.router.navigate([AUTH_LOGIN_URL]);
         return false;
       }
       return true;
     })
   );
  }
}

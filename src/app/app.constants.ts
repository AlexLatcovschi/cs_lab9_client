import { environment } from '../environments/environment';

export const API_URL: string = environment.api_url;

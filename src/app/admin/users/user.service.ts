import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {UserModel} from '../models/user.model';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) {
  }

  search(params?: any): Observable<any> {
    const options: any = {observe: 'response'};
    if (params) {
      options.params = params;
    }
    return this.http.get(environment.api_url + '/api/users', options);
  }

  createUser(user: UserModel) {
    return this.http.post(environment.api_url + '/api/users', user);
  }
}

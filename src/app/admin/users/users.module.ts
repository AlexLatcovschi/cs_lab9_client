import {NgModule} from '@angular/core';
import {routedComponents, UsersRoutingModule} from './users-routing.module';
import {ThemeModule} from '../../@theme/theme.module';
import {Ng2CompleterModule} from 'ng2-completer';
import {NbCardModule, NbDialogModule, NbInputModule, NbToastrModule} from '@nebular/theme';
import {LightboxModule} from 'ngx-lightbox';
import {SharedModule} from '../shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {UserService} from './user.service';

@NgModule({
  imports: [
    ThemeModule,
    UsersRoutingModule,
    Ng2CompleterModule,
    NbDialogModule.forChild(),
    NbToastrModule.forRoot(),
    LightboxModule,
    SharedModule,
    NbCardModule,
    FormsModule,
    NbInputModule,
    ReactiveFormsModule,
    NgxDatatableModule.forRoot({
      messages: {
        emptyMessage: 'No data to display', // Message to show when array is presented, but contains no values
        totalMessage: 'total', // Footer total message
        selectedMessage: 'selected' // Footer selected message
      }
    }),
  ],
  declarations: [...routedComponents],
  providers: [UserService]
})
export class UsersModule {
}

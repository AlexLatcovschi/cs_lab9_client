import {Component, OnDestroy, OnInit} from '@angular/core';
import {QueryParamsService} from '../../shared/query-params.service';
import {FormControl} from '@angular/forms';
import {debounceTime, map, switchMap, takeUntil} from 'rxjs/operators';
import {Observable, Subject} from 'rxjs';
import {ColumnMode} from '@swimlane/ngx-datatable';
import {UserService} from './user.service';
import {NbDialogService} from '@nebular/theme';
import {CreateUsersComponent} from './create-users/create-users.component';

@Component({
  selector: 'app-users',
  styleUrls: ['users-styles.scss'],
  template: `
    <nb-card>
      <nb-card-header>
        Users
      </nb-card-header>

      <nb-card-body>
        <div class="input-group mb-3">
          <input nbInput fieldSize="small" class="search" type="text"
                 placeholder="Search..." [formControl]="searchControl">
          <button class="mt-2 createButton btn btn-primary" (click)="openAddUserDialog()">
            Add user
          </button>
        </div>
        <ngx-datatable
          class="bootstrap"
          [rows]="rows"
          [columns]="columns"
          [columnMode]="ColumnMode.force"
          [headerHeight]="50"
          [footerHeight]="50"
          [rowHeight]="50"
          [externalPaging]="true"
          [count]="totalPages"
          [offset]="page.pageNumber"
          [limit]="page.size"
          (page)="setPage($event)"
        >
        </ngx-datatable>
      </nb-card-body>
    </nb-card>

  `
})
export class UsersComponent implements OnInit, OnDestroy {
  public rows: Observable<any[]>;
  public columns = [{name: 'ID'}, {name: 'First Name'}, {name: 'Last Name'}, {name: 'Email'}, {name: 'Activated'}];
  public ColumnMode = ColumnMode;
  public searchControl: FormControl;
  private destroySubject: Subject<any> = new Subject();

  public page = {pageNumber: 0, size: 10};

  public totalPages = 0;

  constructor(private service: UserService,
              private queryParamsService: QueryParamsService,
              private dialogService: NbDialogService) {
  }

  ngOnInit(): void {
    this.searchControl = new FormControl('');

    this.queryParamsService.activatedRoute.queryParams
      .pipe(
        switchMap((searchPhrase: any) => {
          return this.service.search(searchPhrase).pipe(map(({body, headers}) => {
            this.rows = body;

            this.totalPages = +headers.get('X-Total-Count');
          }));
        }),
        takeUntil(this.destroySubject)
      ).subscribe();

    this.searchControl.valueChanges
      .pipe(
        debounceTime(200),
        map((searchPhrase: string) => {
          if (typeof searchPhrase === 'string' && searchPhrase.length > 0) {
            return this.queryParamsService.updateParams({
              'filter': `firstName,lastName.contains=${searchPhrase}`,
              page: 0
            });
          } else {
            return this.queryParamsService.deleteParam('filter');
          }
        }),
        takeUntil(this.destroySubject)
      ).subscribe();
  }

  setPage(pageInfo) {
    this.page.pageNumber = pageInfo.offset;
    this.queryParamsService.updateParams({
      page: pageInfo.offset,
      size: pageInfo.pageSize
    });
  }

  ngOnDestroy() {
    this.destroySubject.next(true);
    this.destroySubject.complete();
    this.destroySubject = null;
  }

  openAddUserDialog() {
    this.dialogService.open(CreateUsersComponent, {
      context: {}
    })
      .onClose.subscribe(() => {
      this.service.search().subscribe(res => {
        this.rows = res.body;
      });
    });
  }
}

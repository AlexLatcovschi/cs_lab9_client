import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NbDialogRef, NbDialogService, NbToastrService} from '@nebular/theme';
import {Observable} from 'rxjs';
import {UserService} from '../user.service';

@Component({
  selector: 'app-create-users',
  templateUrl: './create-users.component.html',
  styleUrls: ['./create-users.component.scss'],
})
export class CreateUsersComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder,
              private service: UserService,
              private toastrService: NbToastrService,
              private dialogService: NbDialogService,
              private ref: NbDialogRef<CreateUsersComponent>) {
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      firstName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(50)]],
      lastName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(50)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  get f() {
    return this.registerForm.controls;
  }

  onSubmit(): Observable<any> {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    } else {
      this.service.createUser(this.registerForm.value).subscribe(() => {
        this.showToast('top-right', 'success');
        this.closeDialog();
      }, () => {
        this.errorToast('top-right', 'danger');
      });
    }
  }

  showToast(position, status) {
    this.toastrService.show(
      status || 'success',
      `User added successfully`,
      {position, status});
  }

  errorToast(position, status) {
    this.toastrService.show(
      status || 'danger',
      `An error occurred!`,
      {position, status});
  }

  closeDialog() {
    this.ref.close();
  }
}

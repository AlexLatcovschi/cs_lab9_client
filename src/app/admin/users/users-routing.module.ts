import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UsersComponent} from './users.component';
import {CreateUsersComponent} from './create-users/create-users.component';

const routes: Routes = [
  {
    path: '',
    component: UsersComponent,
    data: {name: 'Users'},
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule {
}

export const routedComponents = [
  UsersComponent, CreateUsersComponent
];

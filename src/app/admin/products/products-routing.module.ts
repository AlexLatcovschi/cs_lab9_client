import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ProductsComponent} from './products.component';
import {CreateUsersComponent} from './create-products/create-users.component';

const routes: Routes = [
  {
    path: '',
    component: ProductsComponent,
    data: {name: 'Products'},
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule {
}

export const routedComponents = [
  ProductsComponent, CreateUsersComponent
];

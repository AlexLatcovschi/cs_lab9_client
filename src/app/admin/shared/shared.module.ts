import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThemeModule } from '../../@theme/theme.module';
import { TableComponent } from './table/table.component';
import { NbSelectModule } from '@nebular/theme';
import { RouterModule } from '@angular/router';


@NgModule({
  imports: [ThemeModule, CommonModule, NbSelectModule, RouterModule],
  exports: [ThemeModule, CommonModule, TableComponent],
  declarations: [TableComponent]
})
export class SharedModule {
}

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';

export interface Data {
  tableCard: string;
}

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  @Input() tableCard: any;
  @Input() showUrl = true;
  @Input() columns;
  @Input() totalCount: Number;
  currentPage;
  @Output() pageChanged = new EventEmitter();
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onItemRemove = new EventEmitter();
  public data: Data;
  public rows: any;
  public pagination = [];
  page = 0;
  selectedPage = 1;
  paginationUI = [];
  showFullNames;
  selectedColumn;
  initialColumns;
  isDesktop;

  constructor(breakpointObserver: BreakpointObserver) {
    breakpointObserver.observe([
      '(max-width: 991px)'
    ]).subscribe(result => {
      this.isDesktop = !result.matches;
    });
  }

  ngOnInit(): void {
    if (this.isDesktop) {
      this.showFullNames = true;
    } else {
      this.showFullNames = false;
      this.tableCard.docs.map(el => {
        el.sliceValue = 20;
      });
    }

    this.rows = this.tableCard.docs;
    if (this.totalCount > 0) {
      this.generatePagination();
    }
    this.initialColumns = JSON.parse(JSON.stringify(this.columns));
    this.selectedColumn = this.initialColumns[this.initialColumns.length - 1].key;
    this.getTableColumns();
  }

  getTableColumns() {
    if (this.isDesktop) {
      this.columns = this.initialColumns;
    } else {
      this.columns = [];
      this.columns = [this.initialColumns[0]];

      this.initialColumns.map(el => {
        if (el.key === this.selectedColumn) {
          this.columns.push(el);
        }
      });
    }
  }

  generatePagination() {
    this.pagination = [];
    this.page = 1;
    for (let i = 0; i < this.totalCount; i++) {
      if ((i % 10) === 0) {
        this.pagination.push({ page: this.page++ });
      }
    }
    if (this.pagination.length > 1) {
      this.generatePaginationUI();
    }
  }

  generatePaginationUI() {
    this.paginationUI = [];
    this.currentPage = this.selectedPage;
    if (!this.currentPage) {
      this.currentPage = 1;
    }
    this.paginationUI.push({ content: this.pagination[0].page });
    if (this.currentPage < 3) {
      for (let i = 1; i < 6; i++) {
        if (this.pagination[i]) {
          this.paginationUI.push({ content: this.pagination[i].page });
        }
      }
      this.paginationUI.push({ content: '...' });
    } else if (this.currentPage >= 3 && this.currentPage <= this.pagination.length - 3) {
      this.paginationUI.push({ content: '...' });
      for (let i = this.currentPage - 2; i < this.currentPage + 2; i++) {
        if (this.pagination[i]) {
          this.paginationUI.push({ content: this.pagination[i].page });
        }
      }
      this.paginationUI.push({ content: '...' });
    } else if (this.currentPage <= this.pagination.length && this.currentPage > this.pagination.length - 6) {
      this.paginationUI.push({ content: '...' });
      for (let i = this.pagination.length - 6; i < this.pagination.length - 1; i++) {
        if (this.pagination[i]) {
          this.paginationUI.push({ content: this.pagination[i].page });
        }
      }
    }
    this.paginationUI.push({ content: this.pagination[this.pagination.length - 1].page });
  }

  onPageSelected(event) {
    this.selectedPage = Number(event.srcElement.innerText);
    this.pageChanged.emit(this.selectedPage);
    this.generatePaginationUI();
  }

  goToPrevious() {
    const currentActionPage = this.selectedPage--;
    if (currentActionPage >= 0) {
      this.pageChanged.emit(currentActionPage);
    }
    this.generatePaginationUI();
  }

  goToNext() {
    const currentActionPage = this.selectedPage++;
    if (currentActionPage <= this.page) {
      this.pageChanged.emit(currentActionPage);
    }
    this.generatePaginationUI();
  }

  showFullName(row, key) {
    if (row.sliceValue === 20) {
      row.sliceValue = row[key].length;
    } else {
      row.sliceValue = 20;
    }
  }

  showFull() {
    if (this.showFullNames) {
      this.tableCard.docs.map(el => {
        el.sliceValue = 20;
      });
    } else {
      this.tableCard.docs.map(el => {
        el.sliceValue = el.fullName.length;
      });
    }
    this.showFullNames = !this.showFullNames;
  }

  removeItem(id) {
    const result = confirm('Are you sure you want delete this?');
    if (result) {
      this.onItemRemove.emit(id);
    }
  }
}

import {NgModule} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {NbAuthJWTInterceptor} from '@nebular/auth';
import {AdminRoutingModule} from './admin-routing.module';
import {ThemeModule} from '../@theme/theme.module';
import {NbMenuModule, NbSidebarModule} from '@nebular/theme';
import {AdminComponent} from './admin.component';
import {AdminUserProfileService} from './admin-user-profile.service';

@NgModule({
  imports: [
    HttpClientModule,
    AdminRoutingModule,
    ThemeModule,
    NbMenuModule,
    NbSidebarModule.forRoot(),
  ],
  declarations: [AdminComponent],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: NbAuthJWTInterceptor, multi: true},
    AdminUserProfileService,
  ]
})
export class AdminModule {
}

import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';

@Injectable()
export class AdminUserProfileService {
  constructor(private httpClient: HttpClient) {
  }

  getCurrentProfile() {
    return this.httpClient.get(environment.api_url + '/api/profile');
  }
}

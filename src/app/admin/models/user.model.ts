import { RoleModel } from './role.model';

export class UserModel {
  firstName?: string;
  lastName?: string;
  email?: string;
  activated?: boolean;
  roles?: RoleModel[];
}

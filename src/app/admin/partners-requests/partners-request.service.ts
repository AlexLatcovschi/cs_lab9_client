import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export class PartnersRequestService {

  constructor(private http: HttpClient) {
  }

  findAllEncrypted(params): Observable<any> {
    return this.http.get(environment.api_url + '/api/partners-request/encrypted', params);
  }

  findAllDecrypted(params): Observable<any> {
    return this.http.get(environment.api_url + `/api/partners-request/decrypted`, params);
  }

  removeItem(id): Observable<any> {
    return this.http.delete(environment.api_url + `/api/partners-request/${id}`);
  }
}

import { Component, OnDestroy, OnInit } from '@angular/core';
import { QueryParamsService } from '../../shared/query-params.service';
import { FormControl } from '@angular/forms';
import { debounceTime, map, switchMap, takeUntil } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { PartnersRequestService } from './partners-request.service';
import { NbDialogService } from '@nebular/theme';
import { NbTokenLocalStorage, NbTokenService } from '@nebular/auth';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-users',
  styleUrls: ['partners-requests-styles.scss'],
  template: `
    <nb-card>
      <nb-card-header>
        Partners Requests
      </nb-card-header>
      <app-table
        [tableCard]="request"
        [columns]="columns"
        [showUrl]="false"
        [totalCount]="request.totalDocs"
        (pageChanged)="getDataByPage($event)"
        (onItemRemove)="removeItem($event)"
        *ngIf="request && request.totalDocs"></app-table>

      <app-table
        [tableCard]="decryptedRequest"
        [columns]="columns"
        [showUrl]="false"
        [totalCount]="decryptedRequest.totalDocs"
        (pageChanged)="getDataByPage($event)"
        (onItemRemove)="removeItem($event)"
        *ngIf="decryptedRequest && decryptedRequest.totalDocs"></app-table>
    </nb-card>
  `
})
export class PartnersRequestComponent implements OnInit, OnDestroy {
  public rows: Observable<any[]>;
  public columns = [
    { name: 'ID', key: '_id' },
    { name: 'Name', key: 'name' },
    { name: 'Company', key: 'company' },
    { name: 'Phone', key: 'phone' },
    { name: 'Email', key: 'email' },
    { name: 'Created At', key: 'createdAt' }
  ];
  public ColumnMode = ColumnMode;
  public searchControl: FormControl;
  private destroySubject: Subject<any> = new Subject();
  private request;
  private decryptedRequest;
  public page = { pageNumber: 0, size: 10 };
  currentPage = 1;
  public totalPages = 0;
  params = {
    page: 1,
    limit: 10
  };

  constructor(private service: PartnersRequestService,
              private queryParamsService: QueryParamsService,
              private dialogService: NbDialogService,
              private tokenService: NbTokenService,
              private cookie: CookieService) {
  }

  ngOnInit(): void {
    this.searchControl = new FormControl('');
    this.getData();
  }

  getData() {
    this.params.page = this.currentPage;
    // localStorage.setItem('ami_auth_token', document.getElementById('auth-token').innerText);
    // localStorage.setItem('aaa', '111');
    // this.cookie.set('ami_auth_token', JSON.parse(document.getElementById('auth-token').innerText));
    setTimeout(() => {
      this.service.findAllEncrypted({ params: this.params }).subscribe(res => {
        this.request = res;
        this.rows = res.docs;
        // this.tokenService.clear();
        console.log('res: ', res);
      });
      this.service.findAllDecrypted({ params: this.params }).subscribe(res => {
        this.decryptedRequest = res;
        this.rows = res.docs;
        // this.tokenService.clear();
        console.log('res: ', res);
      });
    }, 1000);
  }

  setPage(pageInfo) {
    this.page.pageNumber = pageInfo.offset;
    this.queryParamsService.updateParams({
      page: pageInfo.offset,
      size: pageInfo.pageSize
    });
  }

  ngOnDestroy() {
    this.destroySubject.next(true);
    this.destroySubject.complete();
    this.destroySubject = null;
  }

  getDataByPage(event) {
    if (event) {
      this.currentPage = event;
    } else {
      this.currentPage = 1;
    }
    this.getData();
  }

  removeItem(id) {
    console.log('AAA: ', id);
    this.service.removeItem(id).subscribe(() => {
      this.currentPage = 1;
      this.getData();
    });
  }
}

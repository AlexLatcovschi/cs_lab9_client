import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PartnersRequestComponent } from './partners-request.component';

const routes: Routes = [
  {
    path: '',
    component: PartnersRequestComponent,
    data: { name: 'PartnersRequest' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PartnersRequestRoutingModule {
}

export const routedComponents = [
  PartnersRequestComponent
];

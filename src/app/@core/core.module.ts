import {ModuleWithProviders, NgModule, Optional, SkipSelf} from '@angular/core';
import {CommonModule} from '@angular/common';

import {throwIfAlreadyLoaded} from './module-import-guard';
import {AnalyticsService, LayoutService} from './utils';

import {NgxAuthModule} from '../auth/auth.module';
import {NbAuthModule} from '@nebular/auth';


@NgModule({
  imports: [CommonModule, NgxAuthModule],
  exports: [NgxAuthModule, NbAuthModule],
  declarations: []
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }

  static forRoot(): ModuleWithProviders<CoreModule> {
    return {
      ngModule: CoreModule,
      providers: [LayoutService, AnalyticsService],
    };
  }
}

import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ThemeModule} from './@theme/theme.module';
import {CoreModule} from './@core/core.module';
import {NbMenuModule, NbThemeModule} from '@nebular/theme';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [AppComponent],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    ThemeModule.forRoot(),
    CoreModule.forRoot(),    NbMenuModule.forRoot(),
    NbThemeModule.forRoot({name: 'default'}),
    CommonModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'imageVerify' })
export class ImageVerifyPipe implements PipeTransform {

  transform(input: string): boolean {
    if (input) {
      return !!input.match(/\w+\.(jpg|jpeg|gif|svg|png|tiff|bmp)$/gi);
    } else {
      return false;
    }

  }
}
